message = "Life is too short, You need Python, is is is"
version = ' Python 3.6 '

print(message.count('o'))
print(message.find('i'))        # 없다면 -1
print(message.index("i"))       # 없으면 에러
print(",".join(message))        # 문자 사이사이에 ,를 넣은 문자열로 변환
print(message.upper())
print(message.lower())
print(version.lstrip())
print(version.rstrip())
print(version.strip())
print(message.replace("is", "##", 2))

is_alive = True
if is_alive:
    print("Thank god!")
else:
    print("Oh my god!")

alphabet = ['a', 'b', 'c']
a = 'e'
char = a if a in alphabet else None
print(char)

arr = ['a', 'b', 'c']
for index, value in enumerate(arr):
    print(index, value)

for a in set([4, 2, 3]):
    print(a)

for first in range(2, 10):
    for second in range(1, 10):
        print(first * second, end=" ")
    print('')