import trees

dataSet, labels = trees.createDataSet()

shannonEntropy = trees.calcShannonEnt(dataSet)
print("entropy : ", shannonEntropy)