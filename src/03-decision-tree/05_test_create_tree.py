import treePlotter
import trees

dataSet, labels = trees.createDataSet()

myTree = trees.createTree(dataSet, labels)

treePlotter.createPlot(myTree)