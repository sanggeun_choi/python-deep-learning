import trees

dataSet, labels = trees.createDataSet()

bestFeatureIndex = trees.chooseBestFeatureToSplit(dataSet)
print("분할하기 가장 좋은 속성 인덱스 : ", bestFeatureIndex)