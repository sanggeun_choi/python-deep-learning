from math import log
import operator

# 의사결정트리에는 ID3, CART 등 여러가지가 있음
# 여기서는 ID3를 사용
# 9장에서 CART가 나옴

def createDataSet():
    dataSet = [[1, 1, 'yes'],
               [1, 1, 'yes'],
               [1, 0, 'no'],
               [0, 1, 'no'],
               [0, 1, 'no']]

    # no surfacing : 지표면에 닿지 않고 살 수 있는가
    # flippers : 지느러미를 가지고 있는가
    labels = ['no surfacing', 'flippers']

    return dataSet, labels

# dataSet의 Shannon Entropy를 계산하는 함수
# Entropy : label이 얼마나 다양한지를 나타내는 값.
# Shannon Entropy가 높을 수록 데이터가 혼잡하다는 것을 의미
def calcShannonEnt(dataSet):
    numEntries = len(dataSet)
    labelCounts = {}

    # 각 label의 개수를 계산
    for featVec in dataSet:
        currentLabel = featVec[-1]
        if currentLabel not in labelCounts.keys():
            labelCounts[currentLabel] = 0
        labelCounts[currentLabel] += 1

    # dataSet의 Entorpy 계산
    shannonEnt = 0.0
    for key in labelCounts:
        prob = float(labelCounts[key]) / numEntries
        shannonEnt -= prob * log(prob, 2)

    return shannonEnt

# 주어진 속성으로 데이터를 분할하는 함수
# axis 위치의 값이 value인 항목들을 axis 위치값만 제외해서 리스트로 반환
def splitDataSet(dataSet, axis, value):
    retDataSet = []

    for featVec in dataSet:
        if featVec[axis] == value:
            reducedFeatVec = featVec[:axis]             # axis 위치 기준 앞의 원소만 반환
            reducedFeatVec.extend(featVec[axis+1:])     # axis 위치 기준 뒤의 원소들을 추가
            retDataSet.append(reducedFeatVec)

    return retDataSet

# 데이터 분할 시 가장 좋은 속성의 인덱스를 찾는 함수
# 데이터 분할 시 가장 좋은 속성 : entropy가 낮은 feature
def chooseBestFeatureToSplit(dataSet):
    numFeatures = len(dataSet[0]) - 1
    baseEntropy = calcShannonEnt(dataSet)
    bestInfoGain = 0.0
    bestFeature = -1

    for i in range(numFeatures):
        featList = [example[i] for example in dataSet]
        uniqueVals = set(featList)
        newEntropy = 0.0

        # subDataSet의 entorpy를 계산
        for value in uniqueVals:
            subDataSet = splitDataSet(dataSet, i, value)
            prob = len(subDataSet) / float(len(dataSet))
            newEntropy += prob * calcShannonEnt(subDataSet)

        # 정보이득값을 계산
        infoGain = baseEntropy - newEntropy
        if (infoGain > bestInfoGain):
            bestInfoGain = infoGain
            bestFeature = i

    return bestFeature

# 가장 많은 label값을 반환하는 함수
def majorityCnt(classList):
    classCount={}

    # 각 label의 개수를 계산
    for vote in classList:
        if vote not in classCount.keys():
            classCount[vote] = 0
        classCount[vote] += 1

    sortedClassCount = sorted(classCount.items(), key=operator.itemgetter(1), reverse=True)

    return sortedClassCount[0][0]

# 의사결정트리를 생성하는 함수
def createTree(dataSet, labels):
    classList = [example[-1] for example in dataSet]
    copiedLabels = labels[:]

    if classList.count(classList[0]) == len(classList):
        return classList[0]
    if len(dataSet[0]) == 1:
        return majorityCnt(classList)

    bestFeat = chooseBestFeatureToSplit(dataSet)
    bestFeatLabel = copiedLabels[bestFeat]
    myTree = {bestFeatLabel:{}}

    del(copiedLabels[bestFeat])

    featValues = [example[bestFeat] for example in dataSet]
    uniqueVals = set(featValues)

    for value in uniqueVals:
        myTree[bestFeatLabel][value] = createTree(splitDataSet(dataSet, bestFeat, value), copiedLabels)

    return myTree

# 속성값을 전달해 분류를 수행하는 함수
def classify(inputTree, featLabels, testVec):
    # 루트 노드에 있는 속성명을 저장
    firstStr = list(inputTree.keys())[0]
    # 루트 노드에 연결되어있는 속성값 데이터를 저장
    secondDict = inputTree[firstStr]
    # featLabels에서 루트 노드 속성의 인덱스를 저장
    featIndex = featLabels.index(firstStr)
    # 입력 받은 값에 해당하는 루트 노드 속성의 서브 트리를 저장
    key = testVec[featIndex]
    valueOfFeat = secondDict[key]

    if isinstance(valueOfFeat, dict): 
        classLabel = classify(valueOfFeat, featLabels, testVec)
    else:
        classLabel = valueOfFeat

    return classLabel

def storeTree(inputTree,filename):
    import pickle
    fw = open(filename,'w')
    pickle.dump(inputTree,fw)
    fw.close()
    
def grabTree(filename):
    import pickle
    fr = open(filename)
    return pickle.load(fr)