import trees

dataSet, labels = trees.createDataSet()

dataSet1 = trees.splitDataSet(dataSet, 0, 0)
print("0번째 속성값이 0인 dataSet의 분할 결과 : ", dataSet1)

dataSet2 = trees.splitDataSet(dataSet, 1, 1)
print("1번째 속성값이 1인 dataSet의 분할 결과 : ", dataSet2)