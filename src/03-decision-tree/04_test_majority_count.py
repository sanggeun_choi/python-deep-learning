import trees

dataSet, labels = trees.createDataSet()
classList = [example[-1] for example in dataSet]

label = trees.majorityCnt(classList)
print("가장 많은 label값 : ", label)