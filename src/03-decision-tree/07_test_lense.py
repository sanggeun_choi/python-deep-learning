import trees
import treePlotter

fr = open("lenses.txt")
lenses = [inst.strip().split("\t") for inst in fr.readlines()]
labels = ['age', 'prescript', 'astigmatic', 'tearRate']

lensesTree = trees.createTree(lenses, labels)

treePlotter.createPlot(lensesTree)