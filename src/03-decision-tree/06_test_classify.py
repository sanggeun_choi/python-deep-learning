import trees

dataSet, labels = trees.createDataSet()

myTree = trees.createTree(dataSet, labels)

result1 = trees.classify(myTree, labels, [1, 0])
result2 = trees.classify(myTree, labels, [1, 1])

print("0번째 속성이 1, 1번째 속성이 0인 경우 분류 결과 : ", result1)
print("0번째 속성이 1, 1번째 속성이 1인 경우 분류 결과 : ", result2)