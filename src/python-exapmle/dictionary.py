import operator

# dictionary 값 추가하기
a = {1: 'a'}
a[2] = 'b'
print(a)        # {1: 'a', 2: 'b'}

# dictionary 값 제거하기
b = {'name': 'c', 2: 'b', 1: 'a'}
del b[1]
print(b)        # {'name': 'c', 2: 'b'}

# dictionary 값 변경하기
c = {'age1': 15, 'age2': 20}
c['age2'] = 30
print(c['age1'])        # 15
print(c['age2'])        # 30

d = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
print(d.keys())         # dict_keys(['name', 'phone', 'birth']), 일반 리스트와 동일하나 insert, pop, remove, sort 등을 사용할 수 없음

e = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
print(list(e.keys()))   # ['name', 'phone', 'birth']

f = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
print(f.values())       # dict_values(['pey', '01000000000', '1118']), d.keys()와 동일

g = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
print(list(g.values()))   # ['pey', '01000000000', '1118']

h = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
h.clear()
print(h)                    # {}

i = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
print(i.get('name'))        # pey, i.get('name')은 값이 없으면 None을 반환
print(i['name'])            # pey, i['name']은 값이 없으면 오류 발생

j = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
print(j.get('foo', 'bar'))  # foo, foo키에 해당하는 값이 없을 경우 bar라는 값을 반환

k = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
print('name' in k)          # True, 키가 있는지 체크
print('foo' in k)           # False

l = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
l = sorted(l.items(), key=operator.itemgetter(0))
print(l)                    # [('birth', '1118'), ('name', 'pey'), ('phone', '01000000000')], key를 기준으로 정렬

n = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
n = sorted(n.items(), key=operator.itemgetter(0), reverse=True)
print(n)                    # [('phone', '01000000000'), ('name', 'pey'), ('birth', '1118')], key를 기준으로 역순 정렬

m = {'name': 'pey', 'phone': '01000000000', 'birth': '1118'}
m = sorted(m.items(), key=operator.itemgetter(1))
print(m)                    # [('phone', '01000000000'), ('birth', '1118'), ('name', 'pey')], value를 기준으로 정렬


