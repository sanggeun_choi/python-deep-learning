a = [1, 2, 3, 4, 5]
print(a[2:4])           # [3, 4]
print(a[:2])            # [1, 2]
print(a[2:])            # [3, 4, 5]

b = "12345"
print(b[2:4])           # 34

c = [1, 2, 3]
d = [4, 5, 6]
print(c + d)            # [1, 2, 3, 4, 5, 6]

e = [1, 2, 3]
print(e * 3)            # [1, 2, 3, 1, 2, 3, 1, 2, 3]

f = [1, 2, 3]
f[1:2] = ['a', 'b', 'c']
print(f)                # [1, 'a', 'b', 'c', 3]

g = [1, 2, 3, 4, 5, 6]
g[1:3] = []
print(g)                # [1, 4, 5, 6]

h = [1, 2, 3, 4, 5, 6]
del h[1:3]
print(h)                # [1, 4, 5, 6]

i = [1, 2, 3, 4]
i.append(5)
print(i)                # [1, 2, 3, 4 ,5]

j = [1, 2, 3, 4]
j.append([5, 6])
print(j)                # [1, 2, 3, 4, [5, 6]]

k = [1, 5, 4, 2, 3]
k.sort()
print(k)                # [1, 2, 3, 4, 5]

l = [1, 2, 3, 4, 5]
l.reverse()
print(l)                # [5, 4, 3, 2, 1]

m = [1, 2, 3]
print(m.index(3))       # 2
#print(m.index(4))      # error

n = [1, 2, 3]
n.insert(0, 4)
print(n)                # [4, 1, 2, 3]

o = [5, 4, 3, 2, 1]
o.remove(4)
print(o)                # [5, 3, 2, 1]

p = [5, 4, 3, 2, 1]
print(p.pop())          # 1
print(p)                # [5, 4, 3, 2]

q = [5, 4, 3, 2, 1]
print(q.pop(3))         # 2
print(q)                # [5, 4, 3, 1]

r = [1, 2, 3, 4, 1]
print(r.count(1))       # 2

s = [1, 2, 3]
s.extend([4, 5])
print(s)                # [1, 2, 3, 4, 5]

t = [1, 2, 3]
t = t + [4, 5]
print(t)                # [1, 2, 3, 4, 5]

u = [1, 2, 3]
print(u[:])             # [1, 2, 3], 전체 리스트를 복사

# slicing
colors = ["red", "blue", "green"]
print(colors[0:2])      # ['red', 'blue']
print(colors[-2:])      # ['blue', 'green']
print(colors[:])        # ['red', 'blue', 'green'] : 전체 출력
print(colors[-10:10])   # ['red', 'blue', 'green']
print(colors[::2])      # ['red', 'green'] : 앞에서부터 2칸씩 건너뛰며 출력
print(colors[::-1])     # ['green', 'blue', 'red'] : 맨뒤에서부터 1칸씩 앞으로 출력