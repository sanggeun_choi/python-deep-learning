# 연산자
print(3 ** 5)       # 243
print(7 / 2)        # 3.5
print(7 // 2)       # 3
print(7 % 2)        # 1
print(10 == 10.0)   # True
print(1 == True)    # True
print("" == True)   # False

# 형변환
print(float(10))        # 10.0
print(int(10.7))        # 10
print(float("10.7"))    # 10.7
print(str(10.7))        # 10.7

# 콘솔 입출력
temperature = float(input("온도를 입력하세요 : "))
print(temperature)
print(type(temperature))